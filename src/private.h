#ifndef PRIVATE_H
#define PRIVATE_H

const char *soft_ap_ssid = "MyESP32AP";
const char *soft_ap_password = "1234567890";

//AP
IPAddress localIp(192,168,0,1);
IPAddress gatewayAP(192,168,0,1);
IPAddress subnet(255,255,255,0);

//STA
IPAddress serverIP(192,168,8,222);
IPAddress gatewaySTA(192,168,8,1);
IPAddress mask(255,255,255,0);
IPAddress dns(8,8,8,8);

#endif