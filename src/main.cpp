#include <ArduinoOTA.h>
#include "SPIFFS.h"
#include <WiFi.h>
#include "private.h"
#include <EEPROM.h>
#include "ESP32WebServer.h"

#define SSID_LEN 32
#define PASS_LEN 64
#define EEPROM_SIZE SSID_LEN+PASS_LEN+1
ESP32WebServer server(80);
 
//AP
void handle_root();
void handle_connect();
void handle_css_ap();
//STA
void handle_web();
void handle_css_sta();
void handle_disconnect();

String readFile(String filename);   

void setup() {
 
  Serial.begin(115200);
  EEPROM.begin(EEPROM_SIZE);
  if(!SPIFFS.begin(true)){
    Serial.println("An Error has occurred while mounting SPIFFS");
    return;
  }

  
  uint8_t wifi_mode = EEPROM.readByte(0);
  Serial.printf("Remember = %d\n", wifi_mode);
  char ssid_buff[32];
  char pass_buff[64];
  if(wifi_mode){
    String ssid_eeprom = EEPROM.readString(1);
    Serial.println(ssid_eeprom);
    String pass_eeprom = EEPROM.readString(1+SSID_LEN);
    Serial.println(pass_eeprom);
    ssid_eeprom.toCharArray(ssid_buff,32);
    pass_eeprom.toCharArray(pass_buff,64);
  }

  if(wifi_mode){
    // STA MODE
    Serial.println("STA mode");

    WiFi.mode(WIFI_STA);   
    WiFi.begin(ssid_buff, pass_buff);
    WiFi.config(serverIP, gatewaySTA, mask, dns);        //const IP
  
    while(WiFi.status() != WL_CONNECTED) {
      Serial.print(".");
      delay(400);
    }
    Serial.println("Connected to AP");
    server.on("/",HTTP_GET, handle_web);
    server.on("/disconnect_style.css",HTTP_GET, handle_css_sta);
    server.on("/disconnect",HTTP_POST, handle_disconnect);
    server.begin();

  }else{
    //AP mode
    Serial.println("AP mode");

    WiFi.mode(WIFI_STA);
    WiFi.disconnect();
    WiFi.mode(WIFI_AP);
    delay(100);
    
    WiFi.softAPConfig(localIp, gatewayAP, subnet);
    WiFi.softAP(soft_ap_ssid);
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());

    server.on("/",HTTP_GET, handle_root);
    server.on("/connect_style.css",HTTP_GET, handle_css_ap);
    server.on("/connect",HTTP_POST, handle_connect);
    server.begin();
  }
}
 
void loop() {
  server.handleClient();
}






void handle_root(){
  server.send(200, "text/html", readFile("/connect.html"));
}

void handle_css_ap(){
  server.send(200, "text/css", readFile("/connect_style.css"));
}

void handle_connect(){
  String ssid = server.arg("ssid");
  String pass = server.arg("password");
  String remember_me = server.arg("remember_me");
  
  if(remember_me == "on"){
    EEPROM.writeByte(0,1);
    EEPROM.writeString(1,ssid);
    EEPROM.writeString(1+SSID_LEN,pass);
    Serial.printf("zapisano");
    EEPROM.commit();

    ESP.restart();

  } else EEPROM.writeByte(0,0);
  EEPROM.commit();
  
  server.send(200, "text/html", readFile("/connect.html"));
}




void handle_web(){
  server.send(200, "text/html", readFile("/disconnect.html"));
}

void handle_css_sta(){
  server.send(200, "text/css", readFile("/disconnect_style.css"));
}

void handle_disconnect(){
  EEPROM.writeByte(0,0);
  EEPROM.commit();
  server.send(200, "text/plain", "Disconnected");
  ESP.restart();
}



String readFile(String filename)                //from SPIFFS
{
  String text;
  File file = SPIFFS.open(filename, "r");
  while (file.available())
    text += (char)file.read();
  file.close();
  return text;
}
